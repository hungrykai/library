<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>所有的预约记录</title>
</head>
<body>
	<c:forEach items="${allAppoints }" var="appoint">
		<div style="border: solid;width: 300px;margin-top: 5px;">
			预约编号：${appoint.id }<br><br>
			座位Id：${appoint.seatId }<br><br>
			开始时间：${appoint.appointStartTime }<br><br>
			结束时间：${appoint.appointEndTime }<br><br>
			<a href="${pageContext.request.contextPath}/saoma?id=${appoint.id }">扫码签到</a>
			<a href="${pageContext.request.contextPath}/cancelAppoint?id=${appoint.id }">取消预约</a>
		</div>
	</c:forEach>
</body>
</html>