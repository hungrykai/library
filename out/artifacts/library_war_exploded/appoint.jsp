<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 杨开
  Date: 2020/9/12
  Time: 16:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>预约界面</title>
</head>
<body>
<h3>先选择楼层和区域</h3>
<form id="dd" action="${pageContext.request.contextPath}/success.action">
    楼层：
    <select name="floor">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select><br><br>
    区域：
    <select name="area">
        <option value="A">A</option>
        <option value="B">B</option>
        <option value="C">C</option>
        <option value="D">D</option>
        <option value="E">E</option>
    </select><br><br>

    <input type="submit"  value="确定" ><br>
</form>
</body>
</html>
