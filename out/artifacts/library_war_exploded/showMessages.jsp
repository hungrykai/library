<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 杨开
  Date: 2020/10/20
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>图书馆公告</title>
</head>
<body>
<c:if test="${empty messageList}"><h1 align="center">暂无公告！</h1></c:if>
<c:forEach items="${messageList}" var="message">
    <div>
        <h1 align="center">${message.title}</h1>

        <div style="margin: 20px;padding: 10px">&ensp; &emsp;${message.content}</div>
    </div>
    <br>
</c:forEach>
</body>
</html>
