<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div style="width: 330px;height: 160px;border: solid;">
${record.id }
	<span >截止时间：${record.endTime }</span><br><br>
	<span id="">距离离席还有：</span>
	<br><br>
	<button>私发</button><br><br>
	<c:if test="${flag == 1 }"><a href="${pageContext.request.contextPath}/sign?id=${record.id }">离席回来后签到</a></c:if>
	<c:if test="${flag != 1 }"><a href="${pageContext.request.contextPath}/tempLeave?id=${record.id }">临时离席</a></c:if>
	<a href="${pageContext.request.contextPath}/endLeave?id=${record.id }">完全离席</a>
</div>
</body>
</html>