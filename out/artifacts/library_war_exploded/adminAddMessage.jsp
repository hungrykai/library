<%@ page contentType="text/html;charset=UTF-8" language="java" %>
 
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>添加公告</title>
</head>
<body>
<h1>你好！session中的用户id：${user.id }</h1>
<div>
    <h1>添加公告</h1>
    <hr/>
    <form action="${pageContext.request.contextPath}/AdminAddMessage" method="post">
        <div>
            <h1>标题</h1>
            <textarea name="title" rows="10" cols="200" placeholder="Please Input Content"></textarea>
            <h1>正文:</h1>
            <textarea name="content" rows="10" cols="200" placeholder="Please Input Content"></textarea>
        </div>
          <hr/>
        <div>
            <label for="pubDate">发布日期</label>
            <input type="date" name="releaseDate"/>
        </div>
        <div>
            <button type="submit" >提交</button>
        </div>
    </form>
</div>
</body>
</html>
