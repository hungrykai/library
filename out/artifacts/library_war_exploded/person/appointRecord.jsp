<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>预约记录</title>
</head>
<body>
	<c:forEach items="${appointRecords }" var="appoint">
		<div style="border: solid;width: 500px;margin-top: 5px;">
			开始时间：${appoint.appointStartTime }<br>
			结束时间：${appoint.appointEndTime }<br>
			座位号：${appoint.seatId }<br>
			状态：<c:if test="${appoint.state == 1 }">正常</c:if>
			<c:if test="${appoint.state == -1 }">超时未签到</c:if>
		</div>
	</c:forEach>
</body>
</html>