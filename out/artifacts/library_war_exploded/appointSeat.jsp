<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>选择预约的座位</title>
</head>
<body>
<c:if test="${empty seatList}">无预约座位</c:if>
<c:if test="${!empty seatList}">
<c:forEach items="${seatList}" var="seat">
    <div style="width: 200px; height: 200px; border: solid;display: inline-block;float: left;margin-left: 3px;">
        <div>座位编号：${seat.id }</div>
        <div>楼层：${seat.floor }</div>
        <div>区域：${seat.area }</div>
        <div>座位号：${seat.placeId }</div>

        <c:choose>

            <c:when test="${seat.state == 1}">
                <div>座位状态：
                    <h4>可用</h4>
                    <a href="appointTime?seatId=${seat.id}">立即预约</a>
                </div>
            </c:when>

            <c:otherwise>
                <div>座位状态：
                    <h4>禁用</h4>
                </div>
            </c:otherwise>
        </c:choose>

    </div>
</c:forEach>
</c:if>
</body>
</html>