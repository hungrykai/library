<%@ page import="java.util.ArrayList" %>
<%@ page import="cn.edu.hbue.library.model.Seat" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>成功预约</title>
    </head>
    <body>
        <h1>您的预约记录</h1><br>
    <c:forEach items="${appoints}" var="appoint">
            <div style="width: 200px; height: 200px; border: solid;display: inline-block;float: left;margin-left: 3px;">

                <div>座位编号：${appointseat.getId()}</div>
                <div>楼层：${appointseat.getFloor()}</div>
                <div>区域：${appointseat.getArea()}</div>
                <div>座位号：${appointseat.getPlaceId()}</div>
                <div>可用时间段:
                <%-- <%
                    int userId = (int)session.getAttribute("userId");
                    int seatId = (int)session.getAttribute("seatId");
                    System.out.println("userId:"+userId+"seatId:"+seatId);
                %> --%>
                        ${appoint.appointStartTime}:00-${appoint.appointEndTime}:00
                </div>
                <%-- <a href="saoma?userId=<%=userId%>&seatId=<%=seatId%>">扫码就坐</a> --%>
            </div>
    </c:forEach>

    </body>
</html>
