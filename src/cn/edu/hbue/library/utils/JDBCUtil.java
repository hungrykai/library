package cn.edu.hbue.library.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class JDBCUtil {
    public static String url="jdbc:mysql://localhost:3306/library?serverTimezone=GMT%2B8";
    public static String username="root";
    public static String password="123456";
    public static String driverClassName="com.mysql.jdbc.Driver";
    public static BasicDataSource ds = null;

    static {
        ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(url);
    }

    public static DataSource getDataSource() {
        return ds;
    }

    public static Connection getConn() {
        try {
            return ds.getConnection();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    public static void close(Connection conn, Statement st, ResultSet rs) {
        // 5.释放资源
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e2) {
                // TODO: handle exception
                e2.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception e2) {
                // TODO: handle exception
                e2.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (Exception e2) {
                // TODO: handle exception
                e2.printStackTrace();
            }
        }
    }




}

