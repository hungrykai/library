package cn.edu.hbue.library.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {

	// 获取离当前时间的下一个整点时间
	public static Integer getNextHour() {
		LocalDateTime time = LocalDateTime.now().plusHours(1).toLocalDate()
				.atTime(LocalDateTime.now().plusHours(1).getHour(), 0, 0);
		return time.getHour();
	}

	// 获取离当前时间的上一个整点时间
	public static Integer getLastHour() {
		LocalDateTime time = LocalDateTime.now().plusHours(1).toLocalDate()
				.atTime(LocalDateTime.now().plusHours(0).getHour(), 0, 0);
		return time.getHour();
	}
	
	// 求当前时间与指定时间相差的秒数
	public static long getSecondsBetweenNowAndDate(Date date) {
		Instant now = Instant.now();
		long used = ChronoUnit.SECONDS.between(now, date.toInstant());
		return used;
	}
	
	//获取指定date的小时数
	public static Integer getHourOfDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date); // 放入Date类型数据
		return calendar.get(Calendar.HOUR_OF_DAY);
	}
	
	//输入小时，获取整点时间
	//例如输入22 返回 今天的22点整这个时间
	public static Date getDateOfHour(Integer hour) {
		Date time = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(time);
		cal.set(Calendar.HOUR_OF_DAY, hour); //时
		cal.set(Calendar.MINUTE, 0); //分
		cal.set(Calendar.SECOND, 0); //秒
		cal.set(Calendar.MILLISECOND, 0); //毫秒
		Date time2 = cal.getTime();
		return time2;
	}
	
//	public static void main(String[] args) {
//		getDateOfHour(22);
//	}
}
