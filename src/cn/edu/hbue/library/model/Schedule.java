package cn.edu.hbue.library.model;

import java.util.List;

public class Schedule {

	private Integer id;
	
	private Integer seatId;
	
	private List<Integer> time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public List<Integer> getTime() {
		return time;
	}

	public void setTime(List<Integer> time) {
		this.time = time;
	}

	public Schedule(Integer id, Integer seatId, List<Integer> time) {
		super();
		this.id = id;
		this.seatId = seatId;
		this.time = time;
	}

	public Schedule() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Schedule [id=" + id + ", seatId=" + seatId + ", time=" + time + "]";
	}
	
}
