package cn.edu.hbue.library.model;

public class Seat {

	private Integer id;
	
	private Integer floor;
	private String area;
	
	private Integer placeId;
	
	private Integer state;
	
	private Integer scheduleId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Seat(Integer id, Integer floor, String area, Integer placeId, Integer state, Integer scheduleId) {
		super();
		this.id = id;
		this.floor = floor;
		this.area = area;
		this.placeId = placeId;
		this.state = state;
		this.scheduleId = scheduleId;
	}

	@Override
	public String toString() {
		return "Seat [id=" + id + ", floor=" + floor + ", area=" + area + ", placeId=" + placeId + ", state=" + state
				+ ", scheduleId=" + scheduleId + "]";
	}

	public Seat() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
