package cn.edu.hbue.library.model;

import java.util.Date;

public class News {

	//id
	private Integer id;
	
	//匿名消息发送对象
	private Integer userId;
	
	//匿名消息私发日期
	private Date remindDate;
	
	//提示消息的类型
	/**
	 * 1-请保持安静，不要大声喧哗！
	 * 2-请轻拿轻放，降低音量！
	 * 3-请不要在图书馆讨论问题，注意公共场所的秩序！
	 */
	private Integer newsTypes;

	public News(Integer id, Integer userId, Date remindDate, Integer newsTypes) {
		super();
		this.id = id;
		this.userId = userId;
		this.remindDate = remindDate;
		this.newsTypes = newsTypes;
	}

	public News() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getRemindDate() {
		return remindDate;
	}

	public void setRemindDate(Date remindDate) {
		this.remindDate = remindDate;
	}

	public Integer getNewsTypes() {
		return newsTypes;
	}

	public void setNewsTypes(Integer newsTypes) {
		this.newsTypes = newsTypes;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", userId=" + userId + ", remindDate=" + remindDate + ", newsTypes=" + newsTypes
				+ "]";
	}
}
