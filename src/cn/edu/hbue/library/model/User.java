package cn.edu.hbue.library.model;

public class User {

	//主键id
	private Integer id;
	
	//用户名
	private String name;
	
	//用户性别 0男 1女
	private Integer sex;
	
	//用户账号
	private Integer userId;
	
	//密码
	private String password;
	
	//用户角色  0学生  1管理员  2超级管理员
	private Integer role;
	
	//用户积分
	private Integer integral;
	
	//用户状态 0禁用  1可用
	private Integer state;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getIntegral() {
		return integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	

	public User() {
		super();
	}

	public User(Integer id, String name, Integer sex, Integer userId, String password, Integer role, Integer integral,
			Integer state) {
		super();
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.userId = userId;
		this.password = password;
		this.role = role;
		this.integral = integral;
		this.state = state;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", sex=" + sex + ", userId=" + userId + ", password=" + password
				+ ", role=" + role + ", integral=" + integral + ", state=" + state + "]";
	}
}
