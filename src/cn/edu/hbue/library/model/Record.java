package cn.edu.hbue.library.model;

import java.util.Date;

public class Record {

	private Integer id;
	
	private Integer userId;
	
	private Integer seatId;
	
	private Date startTime;
	
	private Date tempTime;
	
	private Date endTime;
	
	/**
	 * 一个人只能就坐一个位子，不可能有多条记录的状态为0
	 * -1-已经处理完的违约记录，临时离席未在规定时间内签到或者离开时没有点临时离席或完全离席刷卡出门的时候被检测到了
	 * 0-正在占用座位
	 * 1-临时离席
	 * 2-完全离席，本条记录已经处理完
	 */
	private Integer state;

	public Record(Integer id, Integer userId, Integer seatId, Date startTime, Date tempTime, Date endTime,
			Integer state) {
		super();
		this.id = id;
		this.userId = userId;
		this.seatId = seatId;
		this.startTime = startTime;
		this.tempTime = tempTime;
		this.endTime = endTime;
		this.state = state;
	}

	public Record() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getTempTime() {
		return tempTime;
	}

	public void setTempTime(Date tempTime) {
		this.tempTime = tempTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Record [id=" + id + ", userId=" + userId + ", seatId=" + seatId + ", startTime=" + startTime
				+ ", tempTime=" + tempTime + ", endTime=" + endTime + ", state=" + state + "]";
	}
	
}
