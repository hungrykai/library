package cn.edu.hbue.library.model;

import java.util.Date;

public class Violation {
	
	//id
	private Integer id;
	
	//扣分人
	private Integer userId;
	
	//减少的分数
	private Integer decIntegral;
	
	//规则代码
	/**
	 * 1-预约成功后超时未签到
	 * 2-就坐成功后中途离席未在规定时间回来签到
	 * 3-使用完座位后没有点离席，导致该座位没有被释放
	 */
	private Integer ruleCode;
	
	//扣分时间
	private Date decTime;

	public Violation(Integer id, Integer userId, Integer decIntegral, Integer ruleCode, Date decTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.decIntegral = decIntegral;
		this.ruleCode = ruleCode;
		this.decTime = decTime;
	}

	public Violation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDecIntegral() {
		return decIntegral;
	}

	public void setDecIntegral(Integer decIntegral) {
		this.decIntegral = decIntegral;
	}

	public Integer getRuleCode() {
		return ruleCode;
	}

	public void setRuleCode(Integer ruleCode) {
		this.ruleCode = ruleCode;
	}

	public Date getDecTime() {
		return decTime;
	}

	public void setDecTime(Date decTime) {
		this.decTime = decTime;
	}

	@Override
	public String toString() {
		return "Violation [id=" + id + ", userId=" + userId + ", decIntegral=" + decIntegral + ", ruleCode=" + ruleCode
				+ ", decTime=" + decTime + "]";
	}
}
