package cn.edu.hbue.library.model;

import java.util.Date;

public class Appoint {

	private Integer id;
	
	private Integer userId;
	
	private Date appointStartTime;
	
	private Date appointEndTime;
	
	private Integer seatId;
	
	//预约的状态 0-未处理  1-已处理的成功预约  -1-已处理的超时预约
	private Integer state;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getAppointStartTime() {
		return appointStartTime;
	}

	public void setAppointStartTime(Date appointStartTime) {
		this.appointStartTime = appointStartTime;
	}

	public Date getAppointEndTime() {
		return appointEndTime;
	}

	public void setAppointEndTime(Date appointEndTime) {
		this.appointEndTime = appointEndTime;
	}

	

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	

	public Appoint(Integer id, Integer userId, Date appointStartTime, Date appointEndTime, Integer seatId,
			Integer state) {
		super();
		this.id = id;
		this.userId = userId;
		this.appointStartTime = appointStartTime;
		this.appointEndTime = appointEndTime;
		this.seatId = seatId;
		this.state = state;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Appoint() {
		super();
	}

	@Override
	public String toString() {
		return "Appoint [id=" + id + ", userId=" + userId + ", appointStartTime=" + appointStartTime
				+ ", appointEndTime=" + appointEndTime + ", seatId=" + seatId + ", state=" + state + "]";
	}

	
}
