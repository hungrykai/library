package cn.edu.hbue.library.model;

import java.sql.Date;


public class Message {
	private Integer id;
	private Integer userId;
	private String title;
 	private String content;
	private Date releaseDate;
	public Message(Integer id, Integer userId, String title, String content, Date releaseDate) {
		super();
		this.id = id;
		this.userId = userId;
		this.title = title;
		this.content = content;
		this.releaseDate = releaseDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", userId=" + userId +
				", title='" + title + '\'' +
				", content='" + content + '\'' +
				", releaseDate=" + releaseDate +
				'}';
	}

	public Message() { }
	
}
