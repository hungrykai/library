package cn.edu.hbue.library.test;


import org.junit.Test;

import cn.edu.hbue.library.dao.SeatDao;
import cn.edu.hbue.library.dao.SeatDaoImp;
import cn.edu.hbue.library.model.Seat;

public class SeatTest {

	@Test
	public void testAddSeat() {
		SeatDao seatDao = new SeatDaoImp();
		for (int i = 50; i < 100; i++) {
			Seat seat = new Seat(1, 1, "A"+i, i, 1, 1);
			System.out.println(seatDao.addSeat(seat));;
		}
	}

	@Test
	public void testDeleteSeat() {
		SeatDao seatDao = new SeatDaoImp();
		for (int i = 0; i < 100; i++) {
			System.out.println(seatDao.deleteSeat(i));;
		}
	}

	@Test
	public void testGetAllSeats() {
		SeatDao seatDao = new SeatDaoImp();
		System.out.println(seatDao.getAllSeats());
	}

	@Test
	public void testUpdateState() {
		SeatDao seatDao = new SeatDaoImp();
		System.out.println(seatDao.updateState(1, 0));
	}

	@Test
	public void testUpdateAllState() {
		SeatDao seatDao = new SeatDaoImp();
		System.out.println(seatDao.updateAllState(1));
	}

	@Test
	public void testGetById() {
		SeatDao seatDao = new SeatDaoImp();
		System.out.println(seatDao.getSeatById(1));
	}


}
