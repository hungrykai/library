package cn.edu.hbue.library.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cn.edu.hbue.library.dao.UserDao;
import cn.edu.hbue.library.dao.UserDaoImp;
import cn.edu.hbue.library.model.User;

public class UserTest {
	public UserDao userDao = new UserDaoImp();

	@Test
	public void addUserTest() {
		for (int i = 0; i < 1000; i++) {
			User user = new User(i, "宋江", i % 2, 18040000 + i, "1235" + i, i % 3, 60 + i, i % 2);
			System.out.println(userDao.addUser(user));
		}
	}

	@Test
	public void updatePasswordTest() {
		for (int i = 1; i < 100; i++) {
			System.out.println(userDao.updatePassword(i, "8888" + i));
		}
	}

	@Test
	public void updateRoleTest() {
		for (int i = 1; i < 100; i++) {
			System.out.println(userDao.updateRole(i, 1));
		}
	}

	@Test
	public void updateIntegralTest() {
		for (int i = 1; i < 100; i++) {
			System.out.println(userDao.updateIntegral(i, 50));
		}
	}

	@Test
	public void updateStateTest() {
		for (int i = 1; i < 100; i++) {
			System.out.println(userDao.updateState(i, 0));
		}
	}

	@Test
	public void getAllUsersTest() {
		List<User> list = userDao.getAllUsers();
		System.out.println(list.size());
//		for (User user : list) {
//			System.out.println(user.getId());
//		}
	}
	
	@Test
	public void getUserByUserIdTest() {
		for (int i = 1; i < 100; i++) {
			System.out.println(userDao.getUserByUserId(18040020+i));
		}
	}
	
	@Test
	public void getUserByIdTest() {
		for (int i = 1; i < 100; i++) {
			System.out.println(userDao.getUserById(i));
		}
	}
	
	@Test
	public void deleteUserByIdTest() {
//		for (int i = 1; i < 100; i++) {
//			System.out.println(userDao.deleteUserById(i));
//		}
		System.out.println(userDao.deleteUserById(999));
	}
	
	@Test
	public void arrTest() {
		List<String> list = new ArrayList<String>();
		list.add("mike");
		list.add("jerry");
		System.out.println(list.get(1));
	}
	
	@Test
	public void getIdByUserIdandPassword() {
//		System.out.println(userDao.getIdByUserIdandPassword(18040213, "12321312"));
//		System.out.println(userDao.getIdByUserIdandPassword(18040213, "123"));
	}

}
