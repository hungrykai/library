package cn.edu.hbue.library.test;

import java.util.Date;

import org.junit.Test;

import cn.edu.hbue.library.dao.NewsDao;
import cn.edu.hbue.library.dao.NewsDaoImp;
import cn.edu.hbue.library.model.News;

public class NewsTest {

	NewsDao newsDao = new NewsDaoImp();
	
	@Test
	public void addNews() {
		News news = null;
		for (int i = 0; i < 100; i++) {
			news = new News(null, i%2+1, new Date(), i%3 + 1);
			newsDao.addNews(news);
		}
	}
	
	@Test
	public void getAllNewsByUserId() {
		System.out.println((newsDao.getAllNewsByUserId(1)).size());
	}
}
