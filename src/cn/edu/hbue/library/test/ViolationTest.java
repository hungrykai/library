package cn.edu.hbue.library.test;

import java.util.Date;

import org.junit.Test;

import cn.edu.hbue.library.dao.ViolationDao;
import cn.edu.hbue.library.dao.ViolationDaoImp;
import cn.edu.hbue.library.model.Violation;

public class ViolationTest {

	ViolationDao violationDao = new ViolationDaoImp();
	@Test
	public void addViolation() {
		Violation violation = null;
		for (int i = 0; i < 100; i++) {
			violation = new Violation(null, 2, 5, i%3 + 1, new Date());
			violationDao.addViolation(violation);
		}
	}
	
	@Test
	public void getAllViolations() {
		System.out.println(violationDao.getAllViolations(1));
	}
}
