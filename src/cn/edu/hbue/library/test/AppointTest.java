package cn.edu.hbue.library.test;


import java.util.Date;

import org.junit.Test;

import cn.edu.hbue.library.dao.AppointDao;
import cn.edu.hbue.library.dao.AppointDaolmp;
import cn.edu.hbue.library.model.Appoint;

public class AppointTest {

	@Test
	public void testInsertAppoint() {
		Date sDate = new Date();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Date eDate = new Date();
		Appoint appoint = new Appoint(1, 120, sDate, eDate, 1, 0);
		AppointDao appointDao = new AppointDaolmp();
		appointDao.insertAppoint(appoint);
	}
	
	
	@Test
	public void getAppointByUserId() {
		AppointDao appointDao = new AppointDaolmp();
		System.out.println(appointDao.getAppointByUserId(120));
	}

	@Test
	public void testDeleteAppointById() {
		AppointDao appointDao = new AppointDaolmp();
		System.out.println(appointDao.deleteAppointById(4));
	}
	
	
	@Test
	public void updateAppointState() {
		AppointDao appointDao = new AppointDaolmp();
		System.out.println(appointDao.updateAppointState(8, -1));
	}
	
	@Test
	public void getNewAppoint() {
		AppointDao appointDao = new AppointDaolmp();
		System.out.println(appointDao.getNewAppoint(100));
	}

}
