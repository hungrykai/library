package cn.edu.hbue.library.service;

import java.util.List;

import cn.edu.hbue.library.dao.UserDao;
import cn.edu.hbue.library.dao.UserDaoImp;
import cn.edu.hbue.library.model.User;

public class UserService {

    UserDao userDao = new UserDaoImp();

    // 增加一个用户
    public boolean addUser(User user) {
        return userDao.addUser(user);
    }

    // 根据用户id来修改密码
    public boolean updatePassword(Integer id, String password) {
        return userDao.updatePassword(id, password);
    }

    // 根据用户id来修改用户角色
    public boolean updateRole(Integer id, Integer role) {
        return userDao.updateRole(id, role);
    }

    // 根据用户id来修改用户积分
    public boolean updateIntegral(Integer id, Integer integral) {
        return userDao.updateIntegral(id, integral);
    }

    // 根据用户id来修改状态
    public boolean updateState(Integer id, Integer state) {
        return userDao.updateState(id, state);
    }

    // 查询所有用户
    public List<User> getAllUsers(){
        return userDao.getAllUsers();
    }

    // 根据userId查询用户
    public User getUserByUserId(Integer userId) {
        return userDao.getUserByUserId(userId);
    }

    // 根据id查询用户
    public User getUserById(Integer id) {
        return userDao.getUserById(id);
    }
    // 根据name password查询用户
    public User getIdByUserIdandPassword(Integer userId, String password, Integer role) {
        return userDao.getIdByUserIdandPassword(userId, password, role);
    }
    // 根据id删除用户
    public boolean deleteUserById(Integer id) {
        return userDao.deleteUserById(id);
    }
}
