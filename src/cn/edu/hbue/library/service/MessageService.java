package cn.edu.hbue.library.service;

import cn.edu.hbue.library.dao.MessageDao;
import cn.edu.hbue.library.dao.MessageDaoImp;
import cn.edu.hbue.library.model.Message;

import java.util.List;

public class MessageService {

    MessageDao messageDao = new MessageDaoImp();

    //得到所有的通告
    public List<Message> GetAllMessage(){
        return messageDao.selectAllMessages();
    }

    //发布一个通告
    public Integer releaseMessage(Message message){
        return messageDao.insertMessage(message);
    }

    //删除通告
    public Integer deleteMessageById(Integer id){
        return messageDao.deleteMessageById(id);
    }

    //更新通告
    public Integer updateMessage(Message message){
        return messageDao.updateMessage(message);
    }

    //根据id得到一个公告
    public Message GetMessage(Integer id){
        return messageDao.GetmessageById(id);
    }

}
