package cn.edu.hbue.library.service;

import java.util.List;

import cn.edu.hbue.library.dao.ViolationDao;
import cn.edu.hbue.library.dao.ViolationDaoImp;
import cn.edu.hbue.library.model.Violation;

public class ViolationService {

	ViolationDao violationDao = new ViolationDaoImp();

	// 查询所有扣分记录
	public List<Violation> getAllViolations(Integer userId){
		return violationDao.getAllViolations(userId);
	}

	// 新增一条扣分记录
	public boolean addViolation(Violation violation) {
		return violationDao.addViolation(violation);
	}
}
