package cn.edu.hbue.library.service;

import java.util.Date;
import java.util.List;

import cn.edu.hbue.library.dao.RecordDao;
import cn.edu.hbue.library.dao.RecordDaoImp;
import cn.edu.hbue.library.model.Record;

public class RecordService {

	RecordDao recordDao = new RecordDaoImp();

	// 新增一条就坐记录
	public boolean addRecord(Record record) {
		return recordDao.addRecord(record);
	}

	// 通过用户的id查询state为指定值的记录
	public Record getRecordById(Integer userId, Integer state) {
		return recordDao.getRecordById(userId, state);
	}

	// 通过id修改记录的状态
	public boolean updateRecordStateById(Integer id, Integer state) {
		return recordDao.updateRecordStateById(id, state);
	}

	// 修改记录tempTime
	public boolean updateRecordTempTime(Integer id, Date tempTime) {
		return recordDao.updateRecordTempTime(id, tempTime);
	}

	// 修改记录endTime
	public boolean updateRecordEndTime(Integer id, Date endTime) {
		return recordDao.updateRecordEndTime(id, endTime);
	}

	// 根据主键查询record
	public Record getRecord(Integer id) {
		return recordDao.getRecord(id);
	}

	// 根据用户的id查询其所有就坐记录
	public List<Record> getRecords(Integer uId) {
		return recordDao.getRecords(uId);
	}

	// 根据用户的id查询其state为0或为1的记录
	public Record getRecordStateZeroOne(Integer uId) {
		return recordDao.getRecordStateZeroOne(uId);
	}
}
