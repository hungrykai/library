package cn.edu.hbue.library.service;

import cn.edu.hbue.library.dao.ScheduleDaoImp;
import java.util.ArrayList;
import java.util.List;

public class ScheduleService {

    //用于点击一个座位后显示所有时间段的空闲/占用状态，通过返回一个简单的数组a
    //下标0代表第一个时间段，以此类推，里面如果存的数是0代表空闲，否则就是装了学号的代表占用
    //传进座位号
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList ShowAll(Integer seatid){
        ArrayList a = new ArrayList();
        ScheduleDaoImp scheduleDao = new ScheduleDaoImp();
        List<Integer> list = new ArrayList<>();
        list = scheduleDao.seatfortime(seatid);
        for(int i = 0; i < list.size(); i++) {
            a.add(list.get(i));
        }
        return a;
    }



    //点击一个时间段进行选定占用,学号导入,choice:123456...
    //成功选择返回true，反之false
    public boolean selectchoice(Integer userid , int choice, int seatid){
        ScheduleDaoImp scheduleDao = new ScheduleDaoImp();
        List<Integer> list = new ArrayList<>();
        list = scheduleDao.seatfortime(seatid);
        //-1原因是choice从1开始，而list从下标0开始
        if(list.get(choice-1) == 0){
            //填充数据
            scheduleDao.inchoice(userid,choice,seatid);
            return true;
        }
        else{
            return false;
        }
    }


    /**
     * 批量填充时间段批量填充时间段
     * @param userid 学号
     * @param startHour  开始的时间对应的索引  1对应填充time_A,一直填充到maxHour的数字对应的time_?，包括maxHour
     * @param maxHour  结束时间对应的索引
     * @param seatid   座位号
     */
    public void insertSomeTime(Integer userid ,Integer startHour,Integer maxHour,Integer seatid) {
    	ScheduleDaoImp scheduleDao = new ScheduleDaoImp();
        for (int i = startHour; i <= maxHour; i++) {
        	scheduleDao.inchoice(userid, i, seatid);
		}
    }

}
