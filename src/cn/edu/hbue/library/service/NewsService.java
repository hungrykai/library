package cn.edu.hbue.library.service;

import java.util.List;

import cn.edu.hbue.library.dao.NewsDao;
import cn.edu.hbue.library.dao.NewsDaoImp;
import cn.edu.hbue.library.model.News;

public class NewsService {
	
	NewsDao newsDao = new NewsDaoImp();

	// 增加一条匿名消息
	public boolean addNews(News news) {
		return newsDao.addNews(news);
	}

	// 查询某用户的所有匿名提醒消息
	public List<News> getAllNewsByUserId(Integer userId){
		return newsDao.getAllNewsByUserId(userId);
	}
}
