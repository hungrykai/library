package cn.edu.hbue.library.service;

import java.util.List;

import cn.edu.hbue.library.dao.AppointDao;
import cn.edu.hbue.library.dao.AppointDaolmp;
import cn.edu.hbue.library.model.Appoint;

public class AppointService {

    AppointDao appointDao = new AppointDaolmp();

    // 新增一条预约记录
    public boolean insertAppoint(Appoint appoint) {
        return appointDao.insertAppoint(appoint);
    }

    // 查询指定用户的所有预约记录
    public List<Appoint> getAppointByUserId(Integer uid) {
        return appointDao.getAppointByUserId(uid);
    }
    
    // 取消预约
    public boolean deleteAppointById(Integer id) {
        return appointDao.deleteAppointById(id);
    }

    // 修改预约的state信息，标记该预约是否是已经被处理过的预约
    public boolean updateAppointState(Integer id, Integer state) {
        return appointDao.updateAppointState(id, state);
    }

 // 查询用户最新的多条未处理的预约记录
    public List<Appoint> getNewAppoints(Integer uid) {
        return appointDao.getNewAppoints(uid);
    }
    
    //根据学号查询处理完的记录 包括成功的预约记录和超时的记录
    public List<Appoint> getSuccessAppointByUserId(Integer uid){
    	return appointDao.getSuccessAppointByUserId(uid);
    }
    
    //根据id查询预约记录
    public Appoint getAppointById(Integer id) {
    	return appointDao.getAppointById(id);
    }
}
