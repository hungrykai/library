package cn.edu.hbue.library.service;

import cn.edu.hbue.library.dao.SeatDao;
import cn.edu.hbue.library.dao.SeatDaoImp;
import cn.edu.hbue.library.model.Seat;
import java.util.ArrayList;
import java.util.List;

public class SeatService {
    private SeatDao seatDao = new SeatDaoImp();

    /*修改座位状态*/
    public void UpdateSeat(Integer SeatId, Integer state){
        seatDao.updateState(SeatId, state);
    }

    //得到所有的位置状态
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List GetAllState(){
        List list = new ArrayList<>();
        for (Seat seat : seatDao.getAllSeats()){
            list.add(seat.getState());
        }
        return list;
    }

    /*批量更改状态*/
    public void UpdateSomeSeat(ArrayList<Integer> listSeatId, Integer StateId){
        for (int i = 0 ; i < listSeatId.size() ; i++){
            seatDao.updateState(listSeatId.get(i), StateId);
        }
    }

    /*根据楼层，区域，座位号查询*/
    public Integer GetId(Integer floor, String area, Integer seatid){
        return seatDao.getId(floor, area, seatid);
    }

    //获取所有的座位信息
    public List<Seat> getAllSeats(){
        return seatDao.getAllSeats();
    }

    public Seat getSeatById(Integer seatId) {
        return seatDao.getSeatById(seatId);
    }

    //根据楼层，区域查询座位对象，返回集合
    public List<Seat> getSeat(Integer floor, String area){
        SeatDaoImp seatDaoImp = new SeatDaoImp();
        List<Seat> seatList = null;
        seatList = seatDaoImp.getSeat(floor,area);
        return seatList;
    }


}
