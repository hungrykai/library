package cn.edu.hbue.library.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.hbue.library.model.Record;
import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.service.RecordService;
import cn.edu.hbue.library.service.ScheduleService;
import cn.edu.hbue.library.service.UserService;
import cn.edu.hbue.library.utils.TimeUtil;

@Controller
public class RecordController {

	@RequestMapping("/tempLeave")
	public String tempLeave(Integer id, Model model) {
		RecordService recordService = new RecordService();
		recordService.updateRecordTempTime(id, new Date());
		recordService.updateRecordStateById(id, 1);
		Record record = recordService.getRecord(id);
		model.addAttribute("record", record);
		model.addAttribute("flag", 1);
		return "record.jsp";
	}

	@RequestMapping("/endLeave")
	public String endLeave(Integer id) {
		RecordService recordService = new RecordService();
		Record record = recordService.getRecord(id);

		// 原来的结束时间
		Date endDate = record.getEndTime();

		// 更新结束时间和状态
		recordService.updateRecordEndTime(id, new Date());
		recordService.updateRecordStateById(id, 2);

		// 释放该座位的时间
		ScheduleService scheduleService = new ScheduleService();
		scheduleService.insertSomeTime(0, TimeUtil.getLastHour() - 7, TimeUtil.getHourOfDate(endDate) - 8,
				record.getSeatId());
		return "WeChatIndex.jsp";
	}

	@RequestMapping("/sign")
	public String sign(Integer id, HttpSession session, Model model) {
		RecordService recordService = new RecordService();
		Record record = recordService.getRecord(id);
		Date tempTime = record.getTempTime();
		long dec = TimeUtil.getSecondsBetweenNowAndDate(tempTime);
		// 离席时间1小时
		// 超时未签到释放座位
		// 模拟超时未签到
//		dec = 3800;
		if (dec > 3600) {
			// 超时
			User user = (User) session.getAttribute("user");
			UserService userService = new UserService();
			// 扣分
			userService.updateIntegral(user.getId(), user.getIntegral() - 5);
			ScheduleService scheduleService = new ScheduleService();
			// 释放座位
			scheduleService.insertSomeTime(0, TimeUtil.getLastHour() - 7,
					TimeUtil.getHourOfDate(record.getEndTime()) - 8, record.getSeatId());
			// 状态改为-1
			recordService.updateRecordStateById(id, -1);

			// 本条就坐记录的截止时间为这个违约的时间
			recordService.updateRecordEndTime(id, new Date());
			return "WeChatIndex.jsp";
		} else {
			// 恢复该座位的占用状态
			recordService.updateRecordStateById(id, 0);
			Record record2 = recordService.getRecord(id);
			model.addAttribute("record", record2);
			// 正常签到
			model.addAttribute("flag", 0);
			return "record.jsp";
		}
	}

	@RequestMapping("/sitRecord")
	public String sitRecord(HttpSession session, Model model) {
		RecordService recordService = new RecordService();
		User user = (User) session.getAttribute("user");
		List<Record> records = recordService.getRecords(user.getId());
		model.addAttribute("records", records);
		return "/person/sitRecord.jsp";
	}

	@RequestMapping("/outCheck")
	public String outCheck(Integer userId, Model model) {
		UserService userService = new UserService();
		User user = userService.getUserByUserId(userId);
		RecordService recordService = new RecordService();
		Record record = recordService.getRecordById(user.getId(), 0);
		if (record == null) {
			model.addAttribute("mes", "检测通过");
		} else {
			// 什么都没点就走了

			// 扣分
			userService.updateIntegral(user.getId(), user.getIntegral() - 5);

			// 释放座位
			ScheduleService scheduleService = new ScheduleService();
			scheduleService.insertSomeTime(0, TimeUtil.getLastHour() - 7,
					TimeUtil.getHourOfDate(record.getEndTime()) - 8, record.getSeatId());

			// 修改就坐记录
			recordService.updateRecordEndTime(record.getId(), new Date());
			recordService.updateRecordStateById(record.getId(), -1);
			model.addAttribute("mes", "违规，未点离席！");

		}
		return "door.jsp";
	}

	@RequestMapping("/studentRecordNow")
	public String studentRecordNow(Model model, HttpSession session) {
		RecordService recordService = new RecordService();
		User user = (User) session.getAttribute("user");
		Record record = recordService.getRecordStateZeroOne(user.getId());
		model.addAttribute("record", record);
		return "record.jsp";
	}
}
