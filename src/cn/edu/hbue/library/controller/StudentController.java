package cn.edu.hbue.library.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.hbue.library.model.Appoint;
import cn.edu.hbue.library.model.News;
import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.model.Violation;
import cn.edu.hbue.library.service.AppointService;
import cn.edu.hbue.library.service.NewsService;
import cn.edu.hbue.library.service.ViolationService;

@Controller
public class StudentController {

	@RequestMapping("/integral")
	public String integral(Model model,HttpSession session) {
		//查询违约记录？
		List<Violation> violations = null;
		ViolationService violationService = new ViolationService();
		User user = (User) session.getAttribute("user");
		Integer userId = user.getId();
		violations = violationService.getAllViolations(userId);
		model.addAttribute("violations", violations);
		return "/person/integral.jsp";
	}
	
	@RequestMapping("/appointRecord")
	public String appointRecord(Model model,HttpSession session) {
		AppointService appointService = new AppointService();
		User user = (User) session.getAttribute("user");
		Integer uid = user.getId();
		List<Appoint> appointRecords = appointService.getSuccessAppointByUserId(uid);
		model.addAttribute("appointRecords", appointRecords);
		System.out.println(appointRecords);
		return "/person/appointRecord.jsp";
	}
	
	@RequestMapping("/news")
	public String news(Model model,HttpSession session) {
		User user = (User) session.getAttribute("user");
		Integer uid = user.getId();
		NewsService newsService = new NewsService();
		List<News> news = newsService.getAllNewsByUserId(uid);
		model.addAttribute("newsList", news);
		return "/person/news.jsp";
	}
}
