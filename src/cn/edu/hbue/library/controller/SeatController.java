package cn.edu.hbue.library.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.hbue.library.model.Appoint;
import cn.edu.hbue.library.model.Record;
import cn.edu.hbue.library.model.Seat;
import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.service.AppointService;
import cn.edu.hbue.library.service.RecordService;
import cn.edu.hbue.library.service.ScheduleService;
import cn.edu.hbue.library.service.SeatService;
import cn.edu.hbue.library.utils.TimeUtil;

//与座位有关的请求

@Controller
public class SeatController {

	private SeatService seatService = new SeatService();

	@RequestMapping("/scanCodeToSit")
	public String seats(Model model) {
		List<Seat> seats = seatService.getAllSeats();
		model.addAttribute("seats", seats);
		return "/scanCodeToSit.jsp";
	}
	
	//直接扫码立即就坐
	@SuppressWarnings("unchecked")
	@RequestMapping("/selectSeatNow")
	public String selectSeatNow(@RequestParam(value = "seatId") Integer seatId,Model model, HttpSession session) {

		User user = (User) session.getAttribute("user");
		List<Appoint> list2 = new ArrayList<>();
		AppointService appointService = new AppointService();
		list2 = appointService.getNewAppoints(user.getId());
		//如果直接扫码的是和未处理预约记录座位是同一个座位，则可以直接扫码坐
		if(list2.size() == 0 || list2.get(0).getSeatId() == seatId) {

			Seat seat = seatService.getSeatById(seatId);
			// 1.判断该座位的state是否为可用状态
			if (seat.getState() == 0) {
				model.addAttribute("error", "该座位不可用");
				return "/scanCodeToSit.jsp";
			} else {
				// 2.判断当前时刻该座位有没有被预约
				ScheduleService scheduleService = new ScheduleService();
				int lastHour = TimeUtil.getLastHour();
				int nextHour = TimeUtil.getNextHour();
				if (lastHour - 8 < 0 || lastHour - 22 >= 0) {
					//早上8点以前，晚上22点以后不能预约
					model.addAttribute("error", "当前不在图书馆开放时间");
					return "/scanCodeToSit.jsp";
				} else {
					lastHour -= 8;
					nextHour -= 8;
					List<Integer> list = scheduleService.ShowAll(seatId);
					if (list.get(lastHour) != 0 || list.get(nextHour) != 0) {
						model.addAttribute("error", "本座位已被预约或占用,无法就坐，请选择其他座位");
						return "/scanCodeToSit.jsp";
					} else {
						//查询最大可用时间
						int maxHour = 0;
						int i = -1;
						for (i = nextHour; i < list.size(); i++) {
							if (list.get(i) != 0) {
								//如果找到了第一个非空的时间
								maxHour = i;
								break;
							}
						}
						//循环完了后面还是空
						if (list.size() == i) {
							maxHour = 14;
						}
						//把时间存入数据库

						scheduleService.insertSomeTime(user.getUserId(), nextHour, maxHour, seatId);
						maxHour += 7;
						model.addAttribute("success", "就坐成功，当前座位可用使用至" + maxHour + "时" + "59分59秒");
						return "/scanCodeToSit.jsp";
					}
				}
			}
		}else{
			model.addAttribute("error", "您已经有预约座位，请在预约座位进行操作!");
			return "/scanCodeToSit.jsp";
		}

	}
	
	@RequestMapping("/sitNow")
	public String sitNow(Model model,Integer nextHour, Integer endHour,HttpSession session,Integer seatId) {
		Integer maxHour = endHour - 8;
		User user = (User) session.getAttribute("user");
		ScheduleService scheduleService = new ScheduleService();
		scheduleService.insertSomeTime(user.getUserId(), nextHour,maxHour, seatId);
		
		//在record中记录
		Record record = new Record(null, user.getId(), seatId, new Date(), null, TimeUtil.getDateOfHour(endHour), 0);
		RecordService recordService = new RecordService();
		recordService.addRecord(record);
		record = recordService.getRecordById(user.getId(), 0);
		model.addAttribute("record", record);
		return "record.jsp";
	}
	
}
