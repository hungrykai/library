package cn.edu.hbue.library.controller;

import cn.edu.hbue.library.model.Message;
import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.service.MessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MessagesController {

    MessageService messageService = new MessageService();

    @RequestMapping("/showoneMessage/{id}")
    public String ShowoneMessage(HttpSession session,@PathVariable Integer id){
        Message message = new Message();
        message = messageService.GetMessage(id);
        session.setAttribute("message",message);
        return "/admin/showoneMessage.jsp";
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping("/showMessage")
    public String ShowMessage(HttpSession session){
        List messageList = new ArrayList();
        messageList = messageService.GetAllMessage();
        session.setAttribute("messageList",messageList );
        return "/admin/showMessages.jsp";
    }

    @RequestMapping("/myadmin")
    public String myadmin(HttpSession session){
    
        return "/admin/adminAddMessage.jsp";
    }

    @RequestMapping("/AdminAddMessage")
    public String AdminAddMessage(Message message, HttpSession session){
        User user = (User) session.getAttribute("user");
        message.setId(user.getId());
        messageService.releaseMessage(message);
        return "forward:/showMessage";
    }

    @RequestMapping("/updatemyadmin")
    public String updatemyadmin(HttpSession session){
        List<Message> messageList = messageService.GetAllMessage();
        session.setAttribute("messageList",messageList );
        return "/admin/updataMessagepage.jsp";
    }

    @RequestMapping("/updateMessagePage")
    public String updateMessage(Integer but, HttpSession session){
        if (but > 0){
            Message message = messageService.GetMessage(but);
            session.setAttribute("message",message);
            return "/admin/updateMessage.jsp";
        }else {
            but += 10000;
            messageService.deleteMessageById(but);
            return "/showMessage";
        }
    }

    @RequestMapping("/updatemessage")
    public String updatemessage(Message message, HttpSession session){
        Message sessionmessage = (Message) session.getAttribute("message");
        message.setUserId(sessionmessage.getUserId());
        message.setId(sessionmessage.getId());
        messageService.updateMessage(message);
        return "/admin/updataMessagepage.jsp";
    }
}
