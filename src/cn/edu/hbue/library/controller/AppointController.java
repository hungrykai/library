package cn.edu.hbue.library.controller;

import cn.edu.hbue.library.dao.AppointDaolmp;
import cn.edu.hbue.library.model.Appoint;
import cn.edu.hbue.library.model.Seat;
import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.model.Violation;
import cn.edu.hbue.library.service.AppointService;
import cn.edu.hbue.library.service.ScheduleService;
import cn.edu.hbue.library.service.ViolationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import cn.edu.hbue.library.service.SeatService;
import cn.edu.hbue.library.utils.TimeUtil;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class AppointController {

	@RequestMapping("/success.action")
	public String success(Seat seat, HttpSession session) {
		SeatService seatService = new SeatService();
		User user = (User) session.getAttribute("user");
		// 信誉积分不够，预约失败
		if (user.getIntegral() < 90) {
			session.setAttribute("Integral", user.getIntegral());
			return "/fail.jsp";
		}

		// 得到指定区域所有座位
		List<Seat> seatList = seatService.getSeat(seat.getFloor(), seat.getArea());

		// 将座位表存到域里面，方便下一步时间信息的查询
		session.setAttribute("seatList", seatList);

		return "/appointSeat.jsp";
	}

	// 座位选取后点击预约按钮,跳到显示可用时间表
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@RequestMapping("/appointTime")
	public String appointTime(Integer seatId, HttpSession session, Model model) {
		List<Appoint> list2 = new ArrayList<>();
		AppointService appointService = new AppointService();
		User user = (User) session.getAttribute("user");
		list2 = appointService.getNewAppoints(user.getId());
		// 如果直接扫码的是和未处理预约记录座位是同一个座位，则可以直接扫码坐

		if (list2.size() == 0 || list2.get(0).getSeatId() == seatId) {

			// 显示空余的
			ScheduleService scheduleService = new ScheduleService();
			ArrayList ScheduleList = scheduleService.ShowAll(seatId);
			ArrayList FreeList = new ArrayList();
			// 获取当前时间
			Calendar calendar = Calendar.getInstance();
			int curhour = calendar.get(calendar.HOUR_OF_DAY);
			for (int i = 0; i < ScheduleList.size(); i++) {
				if (ScheduleList.get(i).equals(0) && i + 8 > curhour) {
					FreeList.add(i);
				}
			}
			session.setAttribute("seatId", seatId);

			SeatService seatService = new SeatService();
			Seat seat = seatService.getSeatById(seatId);
			// 设置预约座位的对象为session
			session.setAttribute("appointseat", seat);

			session.setAttribute("FreeList", FreeList);
			return "/appointTime.jsp";
		} else {
			model.addAttribute("error", "您已经有预约座位，请在预约座位进行操作!");
			return "/scanCodeToSit.jsp";
		}
	}

	@SuppressWarnings("deprecation")
	@RequestMapping("/success1.action")
	public String success1(int[] time, HttpSession session, Model model) {
		ScheduleService scheduleService = new ScheduleService();
		AppointService appointService = new AppointService();
		Appoint appoint = new Appoint();
		// 获取当前年月日
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date()); // 放入Date类型数据

		int year = calendar.get(Calendar.YEAR); // 获取年份
		int month = calendar.get(Calendar.MONTH); // 获取月份
		int day = calendar.get(Calendar.DATE); // 获取日

		// 把预约时间段放入session
		session.setAttribute("timeList", time);

		int starthour = time[0] + 8;
		int endhour = time[time.length - 1] + 9;
		Date startdate = new Date(year - 1900, month, day, starthour, 0, 0);
		Date enddate = new Date(year - 1900, month, day, endhour, 0, 0);

		User user = (User) session.getAttribute("user");
		int userId = user.getUserId();
		int uId = user.getId();
		appoint.setSeatId((Integer) session.getAttribute("seatId"));
		appoint.setUserId(uId);
		appoint.setAppointStartTime(startdate);
		appoint.setAppointEndTime(enddate);
		appoint.setState(0);

		appointService.insertAppoint(appoint);
		for (int i = 0; i < time.length; i++) {
			scheduleService.selectchoice(userId, time[i] + 1, (Integer) session.getAttribute("seatId"));
		}
		model.addAttribute("appointseat", (Seat) session.getAttribute("appointseat"));

		// appoint对象list
		List<Appoint> appoints = appointService.getNewAppoints(uId);
		session.setAttribute("appoints", appoints);
		return "/allAppoints";
	}

	// 获取所有的未处理的预约记录
	@RequestMapping("/allAppoints")
	public String allAppoints(Model model, HttpSession session) {
		AppointService appointService = new AppointService();
		User user = (User) session.getAttribute("user");
		int uid = user.getId();
		List<Appoint> appoints = appointService.getNewAppoints(uid);
		System.out.println(appoints);
		model.addAttribute("allAppoints", appoints);
		return "/allAppoints.jsp";
	}

	// 取消预约
	@RequestMapping("/cancelAppoint")
	public String cancelAppoint(Integer id) {
		AppointService appointService = new AppointService();
		Appoint appoint = appointService.getAppointById(id);
		// 可以正常取消的情况
		Integer seatId = appoint.getSeatId();
		ScheduleService scheduleService = new ScheduleService();
		Integer startHour = 0;
		Integer maxHour = 0;
		// 时间填0
		Date sDate = appoint.getAppointStartTime();
		Date eDate = appoint.getAppointEndTime();
		startHour = TimeUtil.getHourOfDate(sDate) - 7;
		maxHour = TimeUtil.getHourOfDate(eDate) - 8;
		scheduleService.insertSomeTime(0, startHour, maxHour, seatId);
		// 这条预约记录的状态改为1，表示已经成功处理过的预约
		appointService.updateAppointState(id, 1);
		return "/allAppoints";
	}

	@SuppressWarnings("deprecation")
	@RequestMapping("/saoma") // 扫码模拟
	// id为appoint的id
	public ModelAndView show(HttpSession session, Integer id) {

		AppointService appointService = new AppointService();
		ModelAndView modelAndView = new ModelAndView();
		// 请求过来后, 跳转到别一个界面 往别一个界面当中传一些数据
		User user = (User) session.getAttribute("user");
		int userId = user.getId();
		Appoint appoint = appointService.getAppointById(id);
		int seatId = appoint.getSeatId();
		modelAndView.addObject("seatId", seatId);
		modelAndView.setViewName("saoresult.jsp");

		// 数据比对，获取最近一条未处理的预约记录
		AppointDaolmp appointDaoImp = new AppointDaolmp();

		long a = (appoint.getAppointStartTime()).getTime();
		long h = a / 1000 / 3600;// 小时,模糊的，不用具体，单纯大小就可以
		long m = a / 1000 % 3600 / 60;// 分钟数

		Date date = new Date();
		long b = date.getTime();
		long h2 = b / 1000 / 3600;// 小时
		long m2 = b / 1000 % 3600 / 60;// 分钟数
		// 判断预约的座位是否是当前扫码的座位
		// 清一色来的人都要先预约下座位,再扫码就坐
		if (appoint.getSeatId() == seatId) {
			// 新建一条违约记录
			Violation violation = new Violation();
			ViolationService violationService = new ViolationService();
			if (h2 > h) {
				// 2,预约成功后，超时未签到
				// 增加违约记录
				violation.setUserId(userId);
				violation.setDecIntegral(5);
				violation.setDecTime(date);
				violation.setRuleCode(1);
				violationService.addViolation(violation);

				// 释放座位
				ScheduleService scheduleService = new ScheduleService();
				Integer startHour = 0;
				Integer maxHour = 0;
				// 时间填0
				Date sDate = appoint.getAppointStartTime();
				Date eDate = appoint.getAppointEndTime();
				startHour = TimeUtil.getHourOfDate(sDate) - 7;
				maxHour = TimeUtil.getHourOfDate(eDate) - 8;
				scheduleService.insertSomeTime(0, startHour, maxHour, seatId);

				appointDaoImp.punishbyId(userId);
				modelAndView.addObject("message", "未在预约规定时间扫码，扣取5分信用积分");
				appointService.updateAppointState(id, -1);
			} else if (h2 == h && m2 - m > 10) {
				// 3，增加违约记录
				violation.setUserId(userId);
				violation.setDecIntegral(5);
				date.setMinutes(10);
				date.setSeconds(0);
				violation.setDecTime(date);
				violation.setRuleCode(1);
				violationService.addViolation(violation);

				// 释放座位
				ScheduleService scheduleService = new ScheduleService();
				Integer startHour = 0;
				Integer maxHour = 0;
				// 时间填0
				Date sDate = appoint.getAppointStartTime();
				Date eDate = appoint.getAppointEndTime();
				startHour = TimeUtil.getHourOfDate(sDate) - 7;
				maxHour = TimeUtil.getHourOfDate(eDate) - 8;
				scheduleService.insertSomeTime(0, startHour, maxHour, seatId);

				appointDaoImp.punishbyId(userId);
				modelAndView.addObject("message", "未在预约规定时间扫码，扣取5分信用积分");
				appointService.updateAppointState(id, -1);
			} else if (h2 < h) {
				modelAndView.addObject("message", "您提前到场，未到预约时间!");
			} else {
				// 5
				modelAndView.addObject("message", "扫码成功！请就坐。");
				appointService.updateAppointState(id, 1);
			}
		} else {
			// 6.都要先预约再扫码就坐
			modelAndView.addObject("message", "此座位并非您的预约座位");
		}

		return modelAndView;
	}

}