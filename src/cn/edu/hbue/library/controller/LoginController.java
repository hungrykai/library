package cn.edu.hbue.library.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.service.UserService;


@Controller
public class LoginController {
	
	// 微信登录
	@RequestMapping(value = "/login")
	public String login(Integer userId, String password, Model model, HttpSession session) {
		UserService userService = new UserService();
		User user = userService.getIdByUserIdandPassword(userId, password, 0);
		if (user == null) {
			model.addAttribute("error", "学号或密码错误！");
			// 学号或密码错误
			return "login.jsp";
		} else {
			session.setAttribute("user", user);
			return "WeChatIndex.jsp";
		}
	}
}
