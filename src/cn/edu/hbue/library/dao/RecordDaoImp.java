package cn.edu.hbue.library.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.edu.hbue.library.model.Record;
import cn.edu.hbue.library.utils.JDBCUtil;

public class RecordDaoImp implements RecordDao {

	@Override
	public boolean addRecord(Record record) {
		QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "INSERT INTO `record`(`user_id`,`seat_id`,`start_time`,`temp_time`,`end_time`,`state`) VALUES(?,?,?,?,?,?)";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, record.getUserId(),record.getSeatId(),record.getStartTime(),record.getTempTime(),record.getEndTime(),record.getState());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
	}

	@Override
	public Record getRecordById(Integer userId,Integer state) {
		QueryRunner qr = new QueryRunner();
		Record record = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT `id`,`user_id` userId,`seat_id` seatId,`start_time` startTime,`temp_time` tempTime,`end_time` endTime,`state` FROM `record` WHERE `user_id` = ? AND `state` = ?";
        try {
        	record = qr.query(connection, sql, new BeanHandler<>(Record.class),userId, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return record;
	}

	@Override
	public boolean updateRecordStateById(Integer id, Integer state) {
		int rows = 0;
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "update record set state = ? where id = ?";
        try {
            rows = queryRunner.update(connection, sql, state, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
	}

	@Override
	public boolean updateRecordTempTime(Integer id, Date tempTime) {
		int rows = 0;
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "UPDATE record SET `temp_time`=? WHERE id = ?";
        try {
            rows = queryRunner.update(connection, sql, tempTime, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
	}

	@Override
	public boolean updateRecordEndTime(Integer id, Date endTime) {
		int rows = 0;
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "UPDATE record SET `end_time`=? WHERE id = ?";
        try {
            rows = queryRunner.update(connection, sql, endTime, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
	}

	@Override
	public Record getRecord(Integer id) {
		QueryRunner qr = new QueryRunner();
		Record record = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT `id`,`user_id` userId,`seat_id` seatId,`start_time` startTime,`temp_time` tempTime,`end_time` endTime,`state` FROM `record` WHERE `id` = ?";
        try {
        	record = qr.query(connection, sql, new BeanHandler<>(Record.class), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return record;
	}

	@Override
	public List<Record> getRecords(Integer uId) {
		QueryRunner qr = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        List<Record> list = null;
        String sql = "SELECT `id`,`user_id` userId,`seat_id` seatId,`start_time` startTime,`temp_time` tempTime,`end_time` endTime,`state` FROM `record` WHERE `user_id` = ? and (`state` = -1 or `state` = 2)";
        try {
        	list = qr.query(connection, sql, new BeanListHandler<>(Record.class),uId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return list;
	}

	@Override
	public Record getRecordStateZeroOne(Integer uId) {
		QueryRunner qr = new QueryRunner();
		Record record = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT `id`,`user_id` userId,`seat_id` seatId,`start_time` startTime,`temp_time` tempTime,`end_time` endTime,`state` FROM `record` WHERE `user_id` = ? and (`state` = 0 or `state` = 1)";
        try {
        	record = qr.query(connection, sql, new BeanHandler<>(Record.class),uId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return record;
	}

}
