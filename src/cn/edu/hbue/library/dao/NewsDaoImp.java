package cn.edu.hbue.library.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.edu.hbue.library.model.News;
import cn.edu.hbue.library.utils.JDBCUtil;

public class NewsDaoImp implements NewsDao {

	@Override
	public boolean addNews(News news) {
		QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "INSERT INTO `news`(`userid`,`remind_date`,`news_types`) VALUES(?,?,?)";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, news.getUserId(), news.getRemindDate(), news.getNewsTypes());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
	}

	@Override
	public List<News> getAllNewsByUserId(Integer userId) {
		QueryRunner runner = new QueryRunner();
        Connection connection = null;
        List<News> list = null;
        String sql = "SELECT `id`,`userid` userID,`remind_date` remindDate,`news_types` newsTypes FROM `news` WHERE `userid` = ?";
        try {
            connection = JDBCUtil.getConn();
            list = runner.query(connection, sql, new BeanListHandler<>(News.class), userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
		return list;
	}

}
