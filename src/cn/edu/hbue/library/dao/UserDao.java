package cn.edu.hbue.library.dao;

import java.util.List;

import cn.edu.hbue.library.model.User;

public interface UserDao {

    //增加一个用户
    public boolean addUser(User user);

    //根据用户id来修改密码
    public boolean updatePassword(Integer id, String password);

    //根据用户id来修改用户角色
    public boolean updateRole(Integer id, Integer role);

    //根据用户id来修改用户积分
    public boolean updateIntegral(Integer id, Integer integral);
    
    // 根据userId password role查询用户
    public User getIdByUserIdandPassword(Integer userId, String password, Integer role);
    
    //根据用户id来修改状态
    public boolean updateState(Integer id, Integer state);

    //查询所有用户
    public List<User> getAllUsers();

    //根据userId查询用户
    public User getUserByUserId(Integer userId);

    //根据id查询用户
    public User getUserById(Integer id);

    //根据id删除用户
    public boolean deleteUserById(Integer id);
}
