package cn.edu.hbue.library.dao;

import java.util.List;

import cn.edu.hbue.library.model.Violation;

public interface ViolationDao {

	//查询所有扣分记录
	public List<Violation> getAllViolations(Integer userId);
	
	//新增一条扣分记录
	public boolean addViolation(Violation violation);
	
}
