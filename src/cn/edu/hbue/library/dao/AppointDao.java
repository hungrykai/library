package cn.edu.hbue.library.dao;


import java.util.List;

import cn.edu.hbue.library.model.Appoint;

public interface AppointDao {

    //新增一条预约记录
    public boolean insertAppoint(Appoint appoint);

    //根据id查询指定用户的所有预约记录
    public List<Appoint> getAppointByUserId(Integer uid);

    //取消预约
    public boolean deleteAppointById(Integer id);

    //修改预约的state信息，标记该预约是否是已经被处理过的预约
    public boolean updateAppointState(Integer id, Integer state);

    //查询用户最新的一条未处理的预约记录
    public Appoint getNewAppoint(Integer uid);

    //违约，根据userid扣取信用分
    public boolean punishbyId(Integer uid);
    
    //查询用户最新所有未处理的预约记录
    public List<Appoint> getNewAppoints(Integer uid);

	public List<Appoint> getSuccessAppointByUserId(Integer uid);

	//根据id查询预约记录
	public Appoint getAppointById(Integer id);
}
