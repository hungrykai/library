package cn.edu.hbue.library.dao;

import java.util.List;

import cn.edu.hbue.library.model.Message;

public interface MessageDao {

	//查询所有的通告
	public List<Message> selectAllMessages();

	// 根据userId查找所有的Message
	public List<Message> selectMessagesByUserId(Integer userId);

	// 插入一个Message 没有id， id在数据库中自动生成
	public Integer insertMessage(Message message);

	//删除通告
	public Integer deleteMessageById(Integer id);

	//更新通告
	public Integer updateMessage(Message message);

	//根据id查询公告
	public Message GetmessageById(Integer id);
}
