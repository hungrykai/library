package cn.edu.hbue.library.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import cn.edu.hbue.library.model.User;
import cn.edu.hbue.library.utils.JDBCUtil;

public class UserDaoImp implements UserDao {

    @Override
    public boolean addUser(User user) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "INSERT INTO `user`(`name`,`sex`,`user_id`,`role`,`integral`,`state`,`password`) VALUES(?,?,?,?,?,?,?)";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, user.getName(),user.getSex(),user.getUserId(),user.getRole(),user.getIntegral(),user.getState(),user.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;

    }

    @Override
    public boolean updatePassword(Integer id, String password) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "UPDATE `user` SET `password`=? WHERE `id`=?";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, password, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public boolean updateRole(Integer id, Integer role) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "UPDATE `user` SET `role`=? WHERE `id`=?";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, role, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public boolean updateIntegral(Integer id, Integer integral) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "UPDATE `user` SET `integral`=? WHERE `id`=?";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, integral, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public boolean updateState(Integer id, Integer state) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "UPDATE `user` SET `state`=? WHERE `id`=?";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, state, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public List<User> getAllUsers() {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        List<User> list = null;
        String sql = "SELECT `id`,`name`,`sex`,`user_id` userId,`role`,`integral`,`state`,`password` FROM `user`";
        try {
            connection = JDBCUtil.getConn();
            list = runner.query(connection, sql, new BeanListHandler<>(User.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return list;
    }

    @Override
    public User getUserByUserId(Integer userId) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        User user = null;
        String sql = "SELECT `id`,`name`,`sex`,`user_id` userId,`role`,`integral`,`state`,`password` FROM `user` WHERE `user_id` = ?";
        try {
            connection = JDBCUtil.getConn();
            user = runner.query(connection, sql, new BeanHandler<>(User.class), userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return user;
    }

    @Override
    public User getUserById(Integer id) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        User user = null;
        String sql = "SELECT `id`,`name`,`sex`,`user_id` userId,`role`,`integral`,`state`,`password` FROM `user` WHERE `id`=?";
        try {
            connection = JDBCUtil.getConn();
            user = runner.query(connection, sql, new BeanHandler<>(User.class), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return user;
    }
    // 根据userId password查询用户是否存在
    @Override
    public User getIdByUserIdandPassword(Integer userId, String password, Integer role) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        User user = null;
        String sql = "SELECT `id`,`name`,`sex`,`user_id` userId,`role`,`integral`,`state`,`password` FROM `user` WHERE `role` = ? and `user_id`=? and `password`=?";
        try {
            connection = JDBCUtil.getConn();
            user = runner.query(connection, sql, new BeanHandler<>(User.class), role, userId, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return user;
    }
    @Override
    public boolean deleteUserById(Integer id) {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "DELETE FROM `user` WHERE `id` = ?";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }


}
