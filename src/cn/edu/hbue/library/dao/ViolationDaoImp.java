package cn.edu.hbue.library.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.edu.hbue.library.model.Violation;
import cn.edu.hbue.library.utils.JDBCUtil;

public class ViolationDaoImp implements ViolationDao {

	@Override
	public List<Violation> getAllViolations(Integer userId) {
		QueryRunner runner = new QueryRunner();
        Connection connection = null;
        List<Violation> list = null;
        String sql = "SELECT `id`,`user_id` userId,`dec_integral` decIntegral,`rule_code` ruleCode,`dec_time` decTime FROM `violation` WHERE `user_id` = ?";
        try {
            connection = JDBCUtil.getConn();
            list = runner.query(connection, sql, new BeanListHandler<>(Violation.class), userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
		return list;
	}

	@Override
	public boolean addViolation(Violation violation) {
		QueryRunner runner = new QueryRunner();
        Connection connection = null;
        int rows = 0;
        String sql = "INSERT INTO `violation`(`user_id`,`dec_integral`,`rule_code`,`dec_time`) VALUES(?,?,?,?)";
        try {
            connection = JDBCUtil.getConn();
            rows = runner.update(connection, sql, violation.getUserId(), violation.getDecIntegral(), violation.getRuleCode(), violation.getDecTime());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
	}

}
