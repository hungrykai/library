package cn.edu.hbue.library.dao;

import java.util.List;

public interface ScheduleDao {
    //1.根据座位号查询所有时段学号,把查到的学号都放入list
    public List<Integer> seatfortime(Integer seatid);

    //2.清空时间段
    public void clear(Integer seatid);

    //3.传输学号和时间段选择，在时间段进行学号的导入,choice:1->time_A,以此类推
    public void inchoice(Integer userid, int choice, int seatid);

    //4.在一个时间段中清空某个学号
    public void clearuserid(int userid, int seatid, int choice);

    //5.根据给定的空时间段查询seatId
    public List<Integer> timeforseat(int choice);


}
