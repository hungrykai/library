package cn.edu.hbue.library.dao;

import java.util.List;

import cn.edu.hbue.library.model.News;

public interface NewsDao {

	//增加一条匿名消息
	public boolean addNews(News news);
	
	//查询某用户的所有匿名提醒消息
	public List<News> getAllNewsByUserId(Integer userId);
}
