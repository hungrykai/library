package cn.edu.hbue.library.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.edu.hbue.library.model.Message;
import cn.edu.hbue.library.utils.JDBCUtil;

public class MessageDaoImp implements MessageDao {
	
	// 查找所有的Message
	public List<Message> selectAllMessages(){
		// 通过数据库路径连接创建操作函数
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		// 查询结果保存
		List<Message> messages = new ArrayList<Message>();
		
		// 设置驼峰命名法
		BeanProcessor bean = new GenerousBeanProcessor();
		RowProcessor processor = new BasicRowProcessor(bean);
		
		try {
			// 查找Message并且将属性值通过驼峰命名法转化之后存入结果数组
			messages = qr.query("select * from message", new BeanListHandler<Message>/*声明查询的内容为数组*/(Message.class/*声明查询数组的每一个结点类*/, processor/*声明转化格式为驼峰命名法*/));
		} catch (SQLException e) { // 异常获取
			// TODO Auto-generated catch block
			System.out.println("selectAllMessages Error!");
			e.printStackTrace();
		}
		return messages;
	}
	
	// 根据userId查找所有的Message
	public List<Message> selectMessagesByUserId(Integer userId){
		// 通过数据库路径连接创建操作函数
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		// 查询结果保存
		List<Message> messages = new ArrayList<Message>();
		
		// 设置驼峰命名法
		BeanProcessor bean = new GenerousBeanProcessor();
		RowProcessor processor = new BasicRowProcessor(bean);
		
		try {
			// 查找Message并且将属性值通过驼峰命名法转化之后存入结果数组
			messages = qr.query("select * from message where user_id=" + userId, new BeanListHandler<Message>/*声明查询的内容为数组*/(Message.class/*声明查询数组的每一个结点类*/, processor/*声明转化格式为驼峰命名法*/));
		} catch (SQLException e) { // 异常获取
			// TODO Auto-generated catch block
			System.out.println("selectAllMessages Error!");
			e.printStackTrace();
		}
		return messages;
	}
	
	
	// 插入一个Message 没有id， id在数据库中自动生成
	public Integer insertMessage(Message message) {
		// 声明结果存储对象
		Integer result = null;
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		// sql语句
		String sql = "insert into message (user_id, title, content, release_date) values (?,?,?,?)";
		// 数组元素用于代替sql文件中的问号‘？’
		Object params[] = {message.getUserId(), message.getTitle(), message.getContent(), message.getReleaseDate()};
		try {
			// 插入数据： 注意insert， delete，update三种sql语句都需要使用update函数，select类型则需要使用query函数
			result = qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public Integer deleteMessageById(Integer id) {
		// 声明结果存储对象
		Integer result = null;
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		// sql语句
		String sql = "delete from message where id=" + id;
		// 数组元素用于代替sql文件中的问号‘？’
		try {
			// 插入数据： 注意insert， delete，update三种sql语句都需要使用update函数，select类型则需要使用query函数
			result = qr.update(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public Integer updateMessage(Message message) {
		// 声明结果存储对象
		Integer result = null;
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		// sql语句
		String sql = "update message set user_id=?, title=?, content=?, release_date=? where id=" + message.getId();
		// 数组元素用于代替sql文件中的问号‘？’
		Object params[] = {message.getUserId(), message.getTitle() , message.getContent(), message.getReleaseDate()};
		try {
			// 插入数据： 注意insert， delete，update三种sql语句都需要使用update函数，select类型则需要使用query函数
			result = qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Message GetmessageById(Integer id) {
		// 通过数据库路径连接创建操作函数
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		// 查询结果保存
		Message message = new Message();

		// 设置驼峰命名法
		BeanProcessor bean = new GenerousBeanProcessor();
		RowProcessor processor = new BasicRowProcessor(bean);

		try {
			// 查找Message并且将属性值通过驼峰命名法转化之后存入结果数组
			message = qr.query("select * from message where id=" + id, new BeanHandler<Message>/*声明查询的内容为数组*/(Message.class/*声明查询数组的每一个结点类*/, processor/*声明转化格式为驼峰命名法*/));
		} catch (SQLException e) { // 异常获取
			// TODO Auto-generated catch block
			System.out.println("selectAllMessages Error!");
			e.printStackTrace();
		}
		return message;
	}
}
