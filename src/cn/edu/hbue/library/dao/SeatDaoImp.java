package cn.edu.hbue.library.dao;

import cn.edu.hbue.library.dao.SeatDao;
import cn.edu.hbue.library.model.Seat;
import cn.edu.hbue.library.utils.JDBCUtil;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import java.sql.*;
import java.util.List;

public class SeatDaoImp implements SeatDao {

    @Override
    // 增加座位
    public boolean addSeat(Seat seat) {
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        int rows = 0;
        String sql = "insert into seat(floor, area, place_id, state, schedule_id) values(?, ?, ?, ?, ?)";
        try {
            rows = queryRunner.update(connection, sql, seat.getFloor(), seat.getArea(), seat.getPlaceId(),
                    seat.getState(), seat.getScheduleId());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    // 删除座位
    public boolean deleteSeat(Integer SeatId) {
        int rows = 0;
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "delete from seat where id = ?";
        try {
            rows = queryRunner.update(connection, sql, SeatId);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    // 得到所有的座位
    public List<Seat> getAllSeats() {
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        // 设置驼峰命名法
        BeanProcessor bean = new GenerousBeanProcessor();
        RowProcessor processor = new BasicRowProcessor(bean);
        String sql = "select * from seat";
        List<Seat> list = null;
        try {
            list = queryRunner.query(connection, sql, new BeanListHandler<Seat>(Seat.class,processor));
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return list;
    }

    @Override
    public boolean updateState(Integer seatId,Integer state) {
        int rows = 0;
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "update seat set state = ? where id = ?";
        try {
            rows = queryRunner.update(connection, sql, state, seatId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public boolean updateAllState(Integer state) {
        String sql = "update seat set state = ?";
        int rows = 0;
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        try {
            rows = queryRunner.update(connection, sql, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public Seat getSeatById(Integer seatId) {
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        BeanProcessor bean = new GenerousBeanProcessor();
        RowProcessor processor = new BasicRowProcessor(bean);
        String sql = "select * from seat where id = ?";
        Seat seat = null;
        try {
            seat =  queryRunner.query(connection, sql, new BeanHandler<Seat>(Seat.class,processor), seatId);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return seat;
    }

    @Override
    public Integer getId(Integer floor, String area, Integer seatid) {
        Connection connection = JDBCUtil.getConn();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select id from seat where floor = ? and area = ? and place_id = ? ";
        Integer Id = -1;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1,floor);
            ps.setString(2,area);
            ps.setInt(3,seatid);
            rs = ps.executeQuery();
            while(rs.next()){
                Id = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection,ps,rs);
        }
        return Id;
    }

    //根据楼层，区域查询座位对象，返回集合
    public List<Seat> getSeat(Integer floor, String area){
        QueryRunner queryRunner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        BeanProcessor bean = new GenerousBeanProcessor();
        RowProcessor processor = new BasicRowProcessor(bean);
        String sql = "select * from seat where floor = ? and area = ?";
        List<Seat> seatList = null;
        try {
            seatList = queryRunner.query(connection, sql, new BeanListHandler<Seat>(Seat.class,processor),floor,area);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return seatList;
    }
}
