package cn.edu.hbue.library.dao;


import cn.edu.hbue.library.model.Appoint;
import cn.edu.hbue.library.utils.JDBCUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class AppointDaolmp implements AppointDao {


    @Override
    public boolean insertAppoint(Appoint appoint) {
        QueryRunner qr = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        int rows = 0;
        String sql = "insert into appoint(user_id,appoint_start_time,appoint_end_time,seat_id,state) value(?,?,?,?,?)";
        try {
            rows = qr.update(connection, sql, appoint.getUserId(), appoint.getAppointStartTime(),appoint.getAppointEndTime(),appoint.getSeatId(),appoint.getState());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return rows > 0;
    }

    @Override
    public List<Appoint> getAppointByUserId(Integer uid) {
        QueryRunner qr = new QueryRunner();
        List<Appoint> appoint = new ArrayList<Appoint>();
        Connection connection = JDBCUtil.getConn();
        String sql = "select id,user_id 'userId',appoint_start_time 'appointStartTime',appoint_end_time 'appointEndTime',seat_id seatId,state from appoint where user_id="+ uid;
        try {
            appoint = qr.query(connection, sql, new BeanListHandler<>(Appoint.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return appoint;

    }


    @Override
    public boolean deleteAppointById(Integer id) {
        QueryRunner runner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "DELETE FROM `appoint` WHERE `id` = ?";
        int rows = 0;
        try {
            rows = runner.update(connection, sql, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows > 0;
    }


    @Override
    public boolean updateAppointState(Integer id,Integer state) {
        QueryRunner runner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        String sql = "UPDATE `appoint` SET `state` = ? WHERE id = ?";
        int rows = 0;
        try {
            rows = runner.update(connection, sql, state, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows > 0;
    }
    @Override
    public Appoint getNewAppoint(Integer uid) {
        QueryRunner qr = new QueryRunner();
        Appoint appoint = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT id,user_id 'userId',appoint_start_time 'appointStartTime',appoint_end_time 'appointEndTime',seat_id seatId,state FROM appoint WHERE `user_id` = ? AND `state` = 0";
        try {
            appoint = qr.query(connection, sql, new BeanHandler<>(Appoint.class),uid);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return appoint;
    }

    @Override
    public boolean punishbyId(Integer uid){
        QueryRunner runner = new QueryRunner();
        Connection connection = JDBCUtil.getConn();
        //一次违规扣五分
        String sql = "update user set integral = integral - 5 WHERE id = ?";
        int rows = 0;
        try {
            rows = runner.update(connection, sql, uid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows > 0;
    }

    @Override
    //查询用户最新所有未处理的预约记录
    public List<Appoint> getNewAppoints(Integer uid) {
        QueryRunner qr = new QueryRunner();
        List<Appoint> appoint = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT id,user_id 'userId',appoint_start_time 'appointStartTime',appoint_end_time 'appointEndTime',seat_id seatId,state FROM appoint WHERE `user_id` = ? AND `state` = 0";
        try {
            appoint = qr.query(connection, sql, new BeanListHandler<>(Appoint.class),uid);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return appoint;
    }

	@Override
	public List<Appoint> getSuccessAppointByUserId(Integer uid) {
		QueryRunner qr = new QueryRunner();
        List<Appoint> appoint = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT id,user_id 'userId',appoint_start_time 'appointStartTime',appoint_end_time 'appointEndTime',seat_id seatId,state FROM appoint WHERE `user_id` = ? AND (`state` = 1 OR `state` = -1)";
        try {
            appoint = qr.query(connection, sql, new BeanListHandler<>(Appoint.class),uid);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return appoint;
	}

	@Override
	public Appoint getAppointById(Integer id) {
		QueryRunner qr = new QueryRunner();
        Appoint appoint = null;
        Connection connection = JDBCUtil.getConn();
        String sql = "SELECT id,user_id 'userId',appoint_start_time 'appointStartTime',appoint_end_time 'appointEndTime',seat_id seatId,state FROM appoint WHERE id = ?";
        try {
            appoint = qr.query(connection, sql, new BeanHandler<>(Appoint.class),id);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection, null, null);
        }
        return appoint;
	}
}
