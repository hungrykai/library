package cn.edu.hbue.library.dao;

import java.util.Date;
import java.util.List;

import cn.edu.hbue.library.model.Record;

public interface RecordDao {

	// 新增一条就坐记录
	boolean addRecord(Record record);

	// 根据主键查询record
	Record getRecord(Integer id);

	// 通过用户的id查询state为指定值的记录
	Record getRecordById(Integer userId, Integer state);

	// 通过id修改记录的状态
	boolean updateRecordStateById(Integer id, Integer state);

	// 修改记录tempTime
	boolean updateRecordTempTime(Integer id, Date tempTime);

	// 修改记录endTime
	boolean updateRecordEndTime(Integer id, Date endTime);

	// 根据用户的id查询其所有就坐记录
	List<Record> getRecords(Integer uId);

	// 根据用户的id查询其state为0或为1的记录
	Record getRecordStateZeroOne(Integer uId);

}
