package cn.edu.hbue.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import cn.edu.hbue.library.utils.JDBCUtil;

public class ScheduleDaoImp implements ScheduleDao {
    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    //根据时间选择返回时间段字节字符串
    public static String getChoice(int choice){
        String s;
        if (choice == 1) {
            s = "time_A";
        } else if (choice == 2) {
            s = "time_B";
        } else if (choice == 3) {
            s = "time_C";
        } else if (choice == 4) {
            s = "time_D";
        } else if (choice == 5) {
            s = "time_E";
        } else if (choice == 6) {
            s = "time_F";
        } else if (choice == 7) {
            s = "time_G";
        } else if (choice == 8) {
            s = "time_H";
        } else if (choice == 9) {
            s = "time_I";
        } else if (choice == 10) {
            s = "time_J";
        } else if (choice == 11) {
            s = "time_K";
        } else if (choice == 12) {
            s = "time_L";
        } else if (choice == 13) {
            s = "time_M";
        } else if (choice == 14) {
            s = "time_N";
        } else {
            s = "null";
        }
        return s;
    }


    //1.根据座位号查询所有时段学号,把查到的学号都放入list
    public List<Integer>seatfortime(Integer seatid){
        conn = JDBCUtil.getConn();
        String sql = "select time_A,time_B,time_C,time_D,time_E,time_F,time_G,time_H,time_I,time_J,time_K,time_L,time_M,time_N from schedule where seat_id = ?";

        List<Integer> list = new ArrayList<>();
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, seatid);
            rs = ps.executeQuery();
            while(rs.next()) {
                //0代表无人预约
                int userid = 0;
                userid = rs.getInt("time_A");
                list.add(userid);
                userid = rs.getInt("time_B");
                list.add(userid);
                userid = rs.getInt("time_C");
                list.add(userid);
                userid = rs.getInt("time_D");
                list.add(userid);
                userid = rs.getInt("time_E");
                list.add(userid);
                userid = rs.getInt("time_F");
                list.add(userid);
                userid = rs.getInt("time_G");
                list.add(userid);
                userid = rs.getInt("time_H");
                list.add(userid);
                userid = rs.getInt("time_I");
                list.add(userid);
                userid = rs.getInt("time_J");
                list.add(userid);
                userid = rs.getInt("time_K");
                list.add(userid);
                userid = rs.getInt("time_L");
                list.add(userid);
                userid = rs.getInt("time_M");
                list.add(userid);
                userid = rs.getInt("time_N");
                list.add(userid);

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            JDBCUtil.close(conn, ps, rs);
        }
        return list;
    }
        @Test
        public void test(){
            ScheduleDaoImp a = new ScheduleDaoImp();
            List<Integer> l = new ArrayList<>();
            l = a.seatfortime(1);
            for(int i =0; i < 14; i++){
                System.out.print(l.get(i)+" ");
            }
        }

    //2.清空时间段
    public void clear(Integer seatid){
        conn = JDBCUtil.getConn();
        String sql = "update schedule set time_A = 0 where seat_id = ?";
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1,seatid);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            JDBCUtil.close(conn, ps, null);
        }
    }
        @Test
        public void t(){
            ScheduleDaoImp a = new ScheduleDaoImp();
            a.clear(1);
            List<Integer> l = new ArrayList<>();
            l = a.seatfortime(1);
            for(int i =0; i < 14; i++){
                System.out.print(l.get(i)+" ");
            }
        }

    //3.传输学号和时间段选择，在时间段进行学号的导入,choice:1->time_A,以此类推
    public void inchoice(Integer userid , int choice, int seatid) {
        conn = JDBCUtil.getConn();
        String s;
        s = ScheduleDaoImp.getChoice(choice);
        String p =  "update schedule set " + s + "= ? where seat_id = ?";
        String sql = p;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, userid);
            ps.setInt(2, seatid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close(conn, ps, null);
        }
    }
    //4.在一个时间段中清空某个学号
    public void clearuserid(int userid, int seatid, int choice){
        conn = JDBCUtil.getConn();
        String s = ScheduleDaoImp.getChoice(choice);
        String sql = "update schedule set "+s+"=? where "+s+" = ?";
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1,0);
            ps.setInt(2,userid);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            JDBCUtil.close(conn,ps,null);
        }
    }
    //5.根据给定的空时间段查询seatId
    public List<Integer> timeforseat(int choice){
        conn = JDBCUtil.getConn();
        List<Integer> list = new ArrayList<>();
        String s = ScheduleDaoImp.getChoice(choice);
        String sql = "select seat_id from schedule where " + s + " = 0";
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                list.add(rs.getInt("seat_id"));
            }
            }catch(SQLException e){
            e.printStackTrace();
        }finally {
            JDBCUtil.close(conn,ps,rs);
        }
        return list;
    }



}
