package cn.edu.hbue.library.dao;

import cn.edu.hbue.library.model.Seat;

import java.util.List;

public interface SeatDao {

    //增加座位
    public boolean addSeat(Seat seat);

    //删除座位
    public boolean deleteSeat(Integer SeatId);

    //查询所有座位
    public List<Seat> getAllSeats();

    //修改座位状态
    public boolean updateState(Integer seatId, Integer state);

    //修改所有座位的状态
    public boolean updateAllState(Integer state);

    //根据id查询座位
    public Seat getSeatById(Integer SeatId);

    //根据楼层，区域，座位号查询
    public Integer getId(Integer floor, String area, Integer seatid);

    //根据楼层，区域查询座位对象，返回集合
    public List<Seat> getSeat(Integer floor, String area);


}
