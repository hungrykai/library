<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:forEach items="${records }" var="record">
		<div style="border: solid; margin-top: 5px;">
			编号：${record.id }
			座位编号：${record.seatId }
			开始时间：${record.startTime }
			结束时间：${record.endTime }
			状态：<c:if test="${record.state==2 }">记录良好</c:if>
			<c:if test="${record.state==-1 }">违约，超时未签到，扣5分</c:if>
		</div>
	</c:forEach>
</body>
</html>