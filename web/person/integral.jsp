<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>信用积分</title>
</head>
<body>
	<h2 align="center">当前信用分：${user.integral }</h2>
	<hr>
	<c:forEach items="${violations }" var="violation">
		<div style="height: 120px;border: solid;margin-top: 5px;">
			违约编号：${violation.id } <br>
			违约扣分：${violation.decIntegral }<br>
			违约原因：<br>
			<c:if test="${violation.ruleCode == 1 }">预约后超时未签到</c:if>
			<c:if test="${violation.ruleCode == 2 }">临时离席未按时签到</c:if>
			<c:if test="${violation.ruleCode == 3 }">完全离席未点击离席</c:if>
			<br>
			违约时间：${violation.decTime }<br>
		</div>	
	</c:forEach>
	
	<hr>
</body>
</html>