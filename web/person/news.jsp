<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>拍一拍</title>
</head>
<body>
	<c:forEach items="${newsList }" var="news">
		<div style="border: solid;margin-top: 5px;height: 100px;">
			消息编号：${news.id }<br>
			消息类型：<br>
			<c:if test="${news.newsTypes == 1 }">[系统提示]请保持安静，不要大声喧哗！</c:if>
			<c:if test="${news.newsTypes == 2 }">[系统提示]请轻拿轻放，降低音量！</c:if>
			<c:if test="${news.newsTypes == 3 }">[系统提示]请不要在图书馆讨论问题，注意公共场所的秩序！</c:if><br>
			提示时间：${news.remindDate }
		</div>
	</c:forEach>
</body>
</html>