<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户登录页面</title>
</head>
<body >
<h2 align="center">用户登录页面</h2>
<form action="${pageContext.request.contextPath}/login">
    <table align="center" border="1">
        <tr>
            <td>学号:</td>
            <td><input type="text" name="userId"></td>
        </tr>
        <tr>
            <td>密码:</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td><input type="submit" value="登录" name="login"></td>
        </tr>
    </table>
</form>
${error }
</body>
</html>
