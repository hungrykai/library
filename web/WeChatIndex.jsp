<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>图书馆预约主界面</title>
</head>
<body>
<h1>登录成功！session中的用户id：${user.id }</h1>
<h2 align="center">首页包含预约界面，直接扫码就坐，系统公告</h2>

	<a href="appoint.jsp">预约界面</a>
	<br><br>
	<a href="${pageContext.request.contextPath}/allAppoints">我的预约
	</a><br><br>
	<a href="${pageContext.request.contextPath}/studentRecordNow">就坐记录</a>
	<br><br>
	<a href="${pageContext.request.contextPath}/showMessage">图书馆公告</a>
	<br><br>
	<a href="${pageContext.request.contextPath}/scanCodeToSit">直接扫码就坐</a>
	<br><br>
	<a href="#">更多</a>
	<br><br>
	<a href="${pageContext.request.contextPath}/person/student.jsp">学生信息界面(包含学生的详细信息，积分等)</a>
</body>
</html>
