<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 杨开
  Date: 2020/10/25
  Time: 21:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/updateMessagePage" method="post" >
    <c:forEach items="${messageList}" var="message" varStatus="messageStatus">
        <ul>
            <li><a href="${pageContext.request.contextPath}/showoneMessage/${message.id}">${message.title}</a></li>
            <li>${message.releaseDate}</li>
            <button name="but" value="${message.id}">修改</button>
            <button name="but" value="${message.id -10000}">删除</button>
        </ul>
        <hr>
    </c:forEach>
</form>
</body>
</html>
