<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>选择时间</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/success1.action">
    <c:if test="${empty FreeList}">该位置当前暂无预约时间</c:if>
    <c:if test="${!empty FreeList}">
        预约时间：<br>
        <c:forEach items="${FreeList}" var="item" varStatus="stat">
            <input type="checkbox" name="time" value="${item}" >  ${item + 8}:00-${item + 9}:00
            <br>
        </c:forEach><br>
    </c:if>
    <c:if test="${!empty FreeList}"><input type="submit" value="预约"></c:if>

</form>
</body>
</html>
