<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>模拟直接扫码就坐</title>
</head>
<body>

<div style="width: 300px;height: 20px;">
	${error }
	${success }
</div>
	<h3>在此页面中，用座位图的方式模拟直接扫码就坐的情况，二维码中的内容为该座位的主键id</h3>
	<c:forEach items="${seats }" var="seat">
		<div style="width: 200px; height: 200px; border: solid;display: inline-block;">
			<div>座位编号：${seat.id }</div>
			<div>楼层：${seat.floor }</div>
			<div>区域：${seat.area }</div>
			<div>座位号：${seat.placeId }</div>
			<div>座位状态：${seat.state }</div>
			<div>时间表：${seat.scheduleId }</div>
			<a href="selectSeatNow?seatId=${seat.id }">立即就坐</a>
		</div>
	</c:forEach>
</body>
</html>