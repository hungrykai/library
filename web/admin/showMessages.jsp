<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: 杨开
  Date: 2020/10/20
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/adminMessage.css">
<title>图书馆通知</title>
</head>
<body>
	<h1 align="center" style="color: #5b8583">图书馆通知</h1>
	<div style="font-weight: 700; color: #5b8583;">管理员：${user.name}</div>
	<c:if test="${empty messageList}">
		<h1 align="center" style="color: #5b8583">暂无公告！</h1>
	</c:if>
	<c:forEach items="${messageList}" var="message">
		<div
			style="margin: 20px; padding: 10px; border-style: solid; border-width: 5px; border-color: #5b8583">
			<h1 align="center">${message.title}</h1>
			<div style="margin: 20px; padding: 10px">&ensp;
				&emsp;${message.content}</div>
			<hr />
		</div>
		<br/>
	</c:forEach>

	<hr />
	<%-- <div align="center">
		<a
			href="${pageContext.request.contextPath}/admin/updataMessagepage.jsp"><button>返回</button></a>
	</div> --%>
</body>
</html>
